# prova-wikidados

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

foi utilizado no projeto o quasar, framework requerido da vaga, Vue Router, Vuex, middlewares de Autenticação, axios para requisições http,
interceptors no axios para explicação do fluxo de 'reautenticação do usuário caso trabalhe com refreshToken.
