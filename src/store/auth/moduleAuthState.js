const getToken = () => {
  return localStorage.getItem("token");
};

export default {
  token: getToken()
};
