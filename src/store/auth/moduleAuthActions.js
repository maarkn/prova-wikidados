import router from "../../router/index";
import api from "../../api/http/api";

export default {
  login({ commit }, { email, password }) {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await api.post("/api/login", { email, password });
        commit("SET_TOKEN", data);
        resolve(data);
      } catch (e) {
        const errorMessage = e?.response?.data?.error || e;
        reject(errorMessage);
      }
    });
  },
  removeAccessToken({ commit }) {
    commit("REMOVE_ACCESS_TOKEN");
  },
  logout({ commit }) {
    return new Promise(resolve => {
      commit("REMOVE_ACCESS_TOKEN");
      resolve("Logout com sucesso!");
      router.push("/");
    });
  }
};
