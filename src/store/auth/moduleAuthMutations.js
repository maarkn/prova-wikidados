import api from "../../api/http/api";

export default {
  SET_TOKEN(state, { token }) {
    api.defaults.headers.common["x-auth-token"] = token;
    state.token = token;
    localStorage.setItem("token", token);
  },
  REMOVE_ACCESS_TOKEN() {
    delete api.defaults.headers.common["x-auth-token"];
    localStorage.removeItem("token");
  }
};
