import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import moduleAuth from "./auth/moduleAuth";

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth: moduleAuth
  }
});
