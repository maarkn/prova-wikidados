import axios from "axios";
import store from "../../store/index";

let fetchingAcessToken = false;

const api = axios.create({
  baseURL: "https://reqres.in/"
});

api.interceptors.request.use(config => {
  if (!config.headers.common["x-auth-token"]) {
    const userAccessToken = localStorage.getItem("token") || store.getters["auth/credentials"];

    config.headers.common["x-auth-token"] = userAccessToken;
  }
  return config;
});

api.interceptors.response.use(
  res => {
    return res;
  },
  error => {
    if (error?.response?.status === 401 && !fetchingAcessToken) {
      fetchingAcessToken = true;
      /**
       * usar alguma lógica para poder buscar um novo token utilizando um refresh token por um exemplo
       */
    }

    if (
      error?.response?.status === 403 ||
      (error?.response?.status === 401 && fetchingAcessToken)
    ) {
      /**
       * alguma lógica para remover o usuário da aplicação pois o mesmo não tem autorizações para ficar nela
       */
    }

    return Promise.reject(error);
  }
);

export default api;
