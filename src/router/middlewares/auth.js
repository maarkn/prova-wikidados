export default function auth({ next, store }) {
  const localStorageToken = localStorage.getItem("token");
  console.log("Store", store);
  if (!store.state.auth.token && !localStorageToken) {
    return next({
      name: "LoginPage"
    });
  }

  return next();
}
