import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import store from "../store/index";

// middlewares
import auth from "./middlewares/auth";
import middlewarePipeline from "./middlewarePipeline";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "LoginPage",
    component: Login
  },
  {
    path: "/register",
    name: "RegisterUser",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "register" */ "../views/Register.vue"),
    meta: {
      middleware: [auth]
    }
  },
  {
    path: "/users",
    name: "UsersList",
    component: () => import(/* webpackChunkName: "user-list" */ "../views/Users.vue"),
    meta: {
      middleware: [auth]
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  try {
    if (!to.meta.middleware) {
      return next();
    }
    const middleware = to.meta.middleware;

    const context = {
      to,
      from,
      next,
      store
    };
    /** função recursiva para poder 'executar' vários middlewares, Ex [auth, isAdmin, isActive], executa seguindo a sequencia do array */
    return middleware[0]({
      ...context,
      next: middlewarePipeline(context, middleware, 1)
    });
  } catch (error) {
    console.error("@@@@", error);
  }
});

export default router;
