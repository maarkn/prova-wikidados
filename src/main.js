import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./quasar";
import api from "./api/http/api";

Vue.config.productionTip = false;

const plugin = {
  install() {
    Vue.api = api;
    Vue.prototype.$api = api;
  }
};

Vue.use(plugin);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
